/**Variables globales*/
const main$$ = document.createElement('main');
const cabecera$$ = document.createElement('div');
let AvatarArray = [0,0,0,0,0];  //Array de divsAvatar por defecto ningún avatar seleccionado
let jugadoresArray = [];
let sum;
let turno = 0;



window.onload = function () { //Una vez que la página está cargada, llama a las funciones
  inicio();
}

function cabecera(){
  //Creación de cabecera
  cabecera$$.classList.add('cabecera');

  const div1$$ = document.createElement('div');
  div1$$.classList.add('cabecera-div1');
  const tituloh1$$ = document.createElement('h1');
  tituloh1$$.classList.add('tituloh1');
  tituloh1$$.textContent = "Escoge tu personaje y su contrincante";
  div1$$.appendChild(tituloh1$$);

  const div2$$ = document.createElement('div');
  div2$$.classList.add('cabecera-div2');
  const buttonReset$$ = document.createElement('button');
  buttonReset$$.classList.add('reset-button');
  buttonReset$$.textContent = "¿Volver a jugar?";
  div2$$.appendChild(buttonReset$$);

  cabecera$$.appendChild(div1$$);
  cabecera$$.appendChild(div2$$);
  document.body.appendChild(cabecera$$);
}

function inicio(){
  cabecera();
  const buttonReset$$ = document.querySelector('.reset-button');
  buttonReset$$.style.display = 'none';

  const tablero$$ = document.createElement('div');
  tablero$$.classList.add('tablero');

  fetch('http://localhost:3000/characters').then(res => res.json()).then(function(characters){
    const buttonLuchar$$ = document.createElement('button');
    buttonLuchar$$.classList.add('luchar-button');
    buttonLuchar$$.textContent = "¡Luchar!";

    buttonLuchar$$.addEventListener('click', function(e) {
      if((sum = AvatarArray.reduce((accumulator, selected) => accumulator + selected, 0))=== 2){
        luchar(characters);
      }else{
        alert("¡Debes seleccionar dos personajes que vayan a la lucha!");
      }
      
    });

    for (const char of characters) {
      const divAvatar$$ = document.createElement('div');
      divAvatar$$.classList.add('divAvatar')
      const Avatar$$ = document.createElement('img');
      Avatar$$.classList.add('imgAvatar')
      const pAvatar$$ = document.createElement('p');

      Avatar$$.src = char.avatar;
      pAvatar$$.textContent = char.name;

      divAvatar$$.appendChild(Avatar$$);
      divAvatar$$.appendChild(pAvatar$$);

      /*Selección de dos personajes ni más ni menos*/
      divAvatar$$.addEventListener('click', function(e) {
        char_Selected(char, divAvatar$$); //AÑADIR EFECTO SOBRE SELECCIONADOS
      });  

      tablero$$.appendChild(divAvatar$$);
      main$$.appendChild(tablero$$);
      main$$.appendChild(buttonLuchar$$);
    }
  });
  document.body.appendChild(main$$);
}

function char_Selected(char, divAvatar$$){
  sum = AvatarArray.reduce((accumulator, selected) => accumulator + selected, 0);
  if (sum === 0){
    AvatarArray[char.id - 1] = 1;
    divAvatar$$.classList.add('avatar-seleccionado');
    
  }else if(sum === 1){
    if(AvatarArray.indexOf(1) + 1 == char.id){ //deseleccionar
      AvatarArray[char.id - 1] = 0;
      divAvatar$$.classList.remove('avatar-seleccionado');
    }else{
      AvatarArray[char.id - 1] = 1;
      divAvatar$$.classList.add('avatar-seleccionado');
    }
  }else{
    console.log(sum);
    if(AvatarArray[char.id-1] === 1){ //deseleccionar
      AvatarArray[char.id - 1] = 0;
      divAvatar$$.classList.remove('avatar-seleccionado');
    }
  }
}

function luchar(characters){
  let count = 0;
  //Juego
  const FirstPlayer = Math.round(Math.random());
  clear();
  cabecera();
  const buttonReset$$ = document.querySelector('.reset-button');
  buttonReset$$.style.display = 'none';
  const tituloh1$$ = document.querySelector('.tituloh1');
  tituloh1$$.textContent = "Batalla";

  //Tablero2
  const tablero$$ = document.createElement('div');
  tablero$$.classList.add('tablero');

  //Divs de las cartas
  for (let i=0; i<AvatarArray.length; i++){
    if(AvatarArray[i] === 1){
      jugadoresArray.push(characters[i]);
    }
  }

  //colocamos el orden de jugada de forma random
  if(FirstPlayer === 1){
    const tmp = jugadoresArray[0];
    jugadoresArray[0] = jugadoresArray[1];
    jugadoresArray[1] = tmp;
  }

  for (let i = 0; i<jugadoresArray.length; i++){
    if(count === 1){
      const divDado$$ = document.createElement('div');
      divDado$$.classList.add('divDado');
      const dadoImg$$ = document.createElement('img');
      dadoImg$$.src = "/public/images/dado.png";
      divDado$$.appendChild(dadoImg$$);
      tablero$$.appendChild(divDado$$);
    }
    const divFoto$$ = document.createElement('div');
      divFoto$$.classList.add('divFotoCarta');
      const divCartaFoto$$ = cartaFoto(jugadoresArray[i]);
      const cartaInfoChar$$ = cartaInfoChar(i);
      divFoto$$.appendChild(divCartaFoto$$);
      divFoto$$.appendChild(cartaInfoChar$$);
      tablero$$.appendChild(divFoto$$);
      count++;
  }
  
  main$$.appendChild(tablero$$);
  document.body.appendChild(main$$);
  jugando();

}

function cartaFoto(char){
  const divFoto$$ = document.createElement('div');
  divFoto$$.classList.add('divFotoLucha');
  const img$$ = document.createElement('img');
  img$$.classList.add('foto-lucha');
  img$$.src = char.avatar;
  divFoto$$.appendChild(img$$);
  return divFoto$$;
}

function cartaInfoChar(index){
  const divCarta$$ = document.createElement('div');
  divCarta$$.classList.add('divCartaLucha' + index);

  const pC$$ = document.createElement('p');
  pC$$.textContent = 'Critic: ' + jugadoresArray[index].critic; //Luego cambiaré estos datos
  const pD$$ = document.createElement('p');
  pD$$.textContent = 'Defense: ' + jugadoresArray[index].defense;
  const pV$$ = document.createElement('p');
  pV$$.textContent = 'Vitality: ' + jugadoresArray[index].vitality;

  divCarta$$.appendChild(pC$$);
  divCarta$$.appendChild(pD$$);
  divCarta$$.appendChild(pV$$);
  return divCarta$$;
}


function jugando(){
  while((jugadoresArray[0].vitality > 0) && (jugadoresArray[1].vitality > 0)){
    //setTimeout(function(){ tirada(turno) }, 2000);
    /*(function() {
      setTimeout(function() {
        tirada(turno);
      }, 2000)
    })*/
    tirada(turno);
  }
}

function tirada(index){
  const damages = jugadoresArray[index].damage;
  const critic =  jugadoresArray[index].critic;
  let dañoTotal = 0;

  for (const damage of damages) {
    const d = damage.indexOf('d');
    const veces = damage.slice(0, d);
    const tipoDado = damage.slice(d+1);
    for(let i=0; i<veces; i++){
      let dado = (Math.floor(Math.random() * tipoDado)) + 1;
      if (dado === critic){
        dado = 2*dado;
      }
      dañoTotal = dañoTotal + dado;
    }
  }
  dañoTotal =  dañoTotal - jugadoresArray[1-index].defense;
  //Modificamos la vitalidad del adversario
  const nuevaVitalidad = jugadoresArray[1-index].vitality - dañoTotal;
  if(nuevaVitalidad > 0){
    jugadoresArray[1-index].vitality = nuevaVitalidad;
  }else{
    jugadoresArray[1-index].vitality = 0;
    alert('Ha ganado el jugador ' + jugadoresArray[turno].name);
  }
  modificaVitality(1-index);
    turno = 1 - turno;
}


function modificaVitality(index){
 // console.log('Vitality de ' + jugadoresArray[index].name + ' : ' + jugadoresArray[index].vitality);
  const divCarta$$ = document.querySelector('.divCartaLucha' + index);
  divCarta$$.childNodes[2].textContent = 'Vitality: ' + jugadoresArray[index].vitality;
  divCarta$$.childNodes[2].style.color = 'red';
}

  
















function clear(){
  const cabecChildren$$ = cabecera$$.childNodes;
  const len = cabecChildren$$.length;
    for(let j=0; j<len; j++){
      cabecChildren$$[0].remove();
    }
  cabecera$$.remove();
  
  const mainChildren$$ = main$$.childNodes;

  if(mainChildren$$.childNodes !== undefined){
    const len = (mainChildren$$.childNodes).length;
    for(let j=0; j<len; j++){
      grandChild$$[0].remove();
    }
  }
  const len2 = mainChildren$$.length;
    for(let j=0; j<len2; j++){
      mainChildren$$[0].remove();
    }
  main$$.remove();

}